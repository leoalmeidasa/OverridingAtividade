
package overriding;

public class Dog extends Animal{
    public Dog(){
      super.Name=this.Name;
      super.Color=this.Color;
    }

    @Override
    public String getName() {
        return "Dog:"+Name;
    }

    @Override
    public void setName(String Name) {
        this.Name = Name;
    }
       
    @Override
    public String getSound(){
        return "-Au-Au !";
    }

    @Override
    public String toString() {
        return    this.getName() 
                + super.getColor() 
                + this.getSound();
    }
    
}
