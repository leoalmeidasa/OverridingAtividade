
package overriding;

public class Bee extends Animal{
    public Bee(){
      super.Name=this.Name;
      super.Color=this.Color;
    }

    @Override
    public String getName() {
        return "Bee:"+Name;
    }

    @Override
    public void setName(String Name) {
        this.Name = Name;
    }
       
    @Override
    public String getSound(){
        return "-Ziiiii !";
    }

    @Override
    public String toString() {
        return    this.getName() 
                + super.getColor() 
                + this.getSound();
    }
    
    
}
