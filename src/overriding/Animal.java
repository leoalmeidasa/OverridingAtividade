
package overriding;

public class Animal {
     protected String Name;
     protected String Color;
     protected String Sound;
     
     public Animal(){
         Name="";
         Color="";
     }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getColor() {
        return "-"+Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getSound() {
        return "-"+Sound;
    }

    @Override
    public String toString() {
        return "Animal:"+"\n"
                + this.getName() 
                + this.getColor() 
                + this.getSound();
    }
    
    
}
