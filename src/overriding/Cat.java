
package overriding;

public class Cat extends Animal {
    public Cat(){
      super.Name=this.Name;
      super.Color=this.Color;
    }

    @Override
    public String getName() {
        return "Cat:"+Name;
    }

    @Override
    public void setName(String Name) {
        this.Name = Name;
    }
       
    @Override
    public String getSound(){
        return "-Miau-Miau !";
    }

    @Override
    public String toString() {
        return    this.getName() 
                + super.getColor() 
                + this.getSound();
    }
    
}
