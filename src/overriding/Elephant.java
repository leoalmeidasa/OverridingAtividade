
package overriding;

public class Elephant extends Animal{
    public Elephant(){
      super.Name=this.Name;
      super.Color=this.Color;
    }

    @Override
    public String getName() {
        return "Elephant:"+Name;
    }

    @Override
    public void setName(String Name) {
        this.Name = Name;
    }
       
    @Override
    public String getSound(){
        return "-Hummm !";
    }

    @Override
    public String toString() {
        return    this.getName() 
                + super.getColor() 
                + this.getSound();
    }
    
}
