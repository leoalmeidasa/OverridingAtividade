package overriding;

import java.util.Scanner;

public class MainOverriding {

    public static void main(String[] args) {
        Animal obj;
        Scanner insert = new Scanner(System.in);
        System.out.print("Enter 1 for Bee, 2 for Cat, 3 for Dog and 4 for Elephant:");
        int option = insert.nextInt();
        switch (option) {
            case 1:
                obj = new Bee();
                System.out.print("Name of Bee:");
                obj.setName(insert.next());
                System.out.print("Name of Color:");
                obj.setColor(insert.next());
                System.out.println(obj.toString());
                break;
            case 2:
                obj = new Cat();
                System.out.print("Name of Cat:");
                obj.setName(insert.next());
                System.out.print("Name of Color:");
                obj.setColor(insert.next());
                System.out.println(obj.toString());
                break;
            case 3:
                obj = new Dog();
                System.out.print("Name of Dog:");
                obj.setName(insert.next());
                System.out.print("Name of Color:");
                obj.setColor(insert.next());
                System.out.println(obj.toString());
                break;
            case 4:
                obj = new Elephant();
                System.out.print("Name of Elephant:");
                obj.setName(insert.next());
                System.out.print("Name of Color:");
                obj.setColor(insert.next());
                System.out.println(obj.toString());
                break;
        }

    }
}
